#pragma once
#include <iostream>
#include "Adress.h"




class Bank {

	string bankName;
	Adress adress;

public:

	Bank();
	Bank(string bankName, Adress adress);
	Bank(string bankName, string street, int houseNumber, int code, string city);

	void display();

};
