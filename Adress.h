#pragma once
#include <string>
#include <iostream>


using namespace std;



class Adress {
	string street;
	int houseNumber;
	int code;
	string city;

public:
	Adress();
	Adress(string street, int houseNumber, int code, string city);


	void setStreet(string);
	void sethouseNumber(int);
	void setCode(int);
	void setCity(string);

	string getStreet();
	int getHouseNumber();
	int getCode();
	string getCity();



	void display();
};
