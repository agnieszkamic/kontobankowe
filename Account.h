#pragma once
#include "Bank.h"
#include "Person.h"

class Account : public Bank {
	string accountNumber;
	float saldo;
	int day, month, year;
	Person owner;

};
Person();
Person(string accountNumber, float saldo, int day, int month, int year, Person owner);
Person(string accountNumber, float saldo, int day, int month, int year, string name, string surname, Adress adress);
Person(string accountNumber, float saldo, int day, int month, int year, string name, string surname, int houseNumber, int code, string city);